import React, { useState } from 'react';
import Branch from "./components/Branch";

const App = () => {

  const [ tree, setTree ] = useState({
    id: 0,
    branch: {
      branches: [],
      leaves: [],
    },
  });

  return (
      <div>
        {
          <Branch branch={{ id: tree.id }}
                  branches={tree.branch.branches}
                  leaves={tree.branch.leaves}
          />
        }
      </div>
  );
};

export default App;