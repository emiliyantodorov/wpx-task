import React, { useState, useEffect } from 'react';
import { genHexString } from "../assets";
import Leaf from "./Leaf";

const Branch = ({ branch, leaves, branches }) => {
  const [ bgColor, setBgColor ] = useState("");
  const [ curBranches, setCurBranches ] = useState(branches);
  const [curLeaves, setCurLeaves] = useState(leaves);

  useEffect(() => {
    setBgColor(genHexString(6));
  }, []);

  const changeInputHandler = evt => {
    setBgColor(evt.target.value);
  };

  const addBranch = () => {
    setCurBranches(prevState => [ ...prevState, {
      id: Math.random() * 1000,
      branch: {
        branches: [],
        leaves: [],
      }
    } ]);
  };

  const addLeaf = () => {
    setCurLeaves(prevState => [ ...prevState, {id: Math.random() * 1000} ]);
  }

  return (
    <>
      <div className="branch"
           style={{ backgroundColor: `#${bgColor}`, padding: "2rem", marginBottom: "1rem" }}
      >
        <span>Branch</span>
        <span>(B {curBranches.length})</span>
        <span>(L 0)</span>
        <form style={{ marginBottom: "1rem" }}>
          <input onChange={changeInputHandler} value={bgColor} type="text" id="color" name="color"
                 placeholder="#cccccc"/>
          <div>
            <button type="button" onClick={addBranch}>Add Branch</button>
            <button type="button" onClick={addLeaf}>Add Leaf</button>
          </div>
        </form>

        {
          curBranches.map(curBranch => <Branch
              key={curBranch.id}
              branch={{ id: curBranch.id }}
              branches={curBranch.branch.branches}
            />
          )
        }
        <div>
          {/*{
            curLeaves.map(curLeaf => <Leaf key={curLeaf.id} />)
          }*/}
        </div>
      </div>
    </>
  );
};

export default Branch;